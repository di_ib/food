import validator from '../utils/validator'

export default {
    async checkerrors(obj) {
      let data
    	try {

    	    let merge = obj
    	    let rules = { email: 'required|email' }

    	    data = await validator.make(merge, rules)

    	} catch (err) {
    	    throw err
    	}
      	return data
    }
}
