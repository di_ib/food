
import validator from '../utils/validator'

export default {
    async checkerrors(obj) {
      let data
    	try {

    	    let merge = obj
    	    let rules = { name: 'required', category: 'required', cost: 'required|number', sale: 'required|number', onhand: 'required', detail: 'required', image: 'required'  }

          data = await validator.make(merge, rules)

    	} catch (err) {
    	    throw err
    	}

      	return data
    }
}
