import emailv from '../models/email'
import api from '@/plugins/api'

export default {
  async checkEmail(input) {

    let data

    try {

      await emailv.checkerrors({email: input})

      await api().get('/auth/register/checkEmail/' + input).then((res) => {

        if(res.data.correct) {

          data = { emailCorrect: true, email: ["You can use this email"] }

        }else {

          data = { emailCorrect: false, email: ["Email does exist"] }

        }

      })

      .catch(err => {

        throw err

      })

    } catch(err) {

      throw err

    }

     return data

  }
}
