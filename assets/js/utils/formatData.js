export default {

  formatDate(input) {

    if(input == undefined) {

      return ""

    }else {

      let date = new Date(input)
      var weekday = new Array(7);
      weekday[0] = 'Sun';
      weekday[1] = 'Mon';
      weekday[2] = 'Tue';
      weekday[3] = 'Wed';
      weekday[4] = 'Thu';
      weekday[5] = 'Fri';
      weekday[6] = 'Sat';

      return weekday[date.getDay()] + ", " + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()
    }

  },
  formatTelephone(input) {

    return input.replace(/(\d{2,})(\d{3})(\d{4})/, '$1-$2-$3')

  },
  formatAccount(input) {

    return input.replace(/(\d{6})(\d{4})(\d{2})(\d{1})/, '$1-$2-$3-$4')

  },
  firstUpperCase(input) {

    return input.charAt(0).toUpperCase() + input.slice(1)

  },
  flattenObject(ob) {
    var toReturn = {};

    for (var i in ob) {
      if (!ob.hasOwnProperty(i)) continue;

      if ((typeof ob[i]) == 'object') {
        var flatObject = this.flattenObject(ob[i]);
        for (var x in flatObject) {
          if (!flatObject.hasOwnProperty(x)) continue;

          // toReturn[i + '.' + x] = flatObject[x];
          toReturn[x] = flatObject[x];
        }
      } else {
        toReturn[i] = ob[i];
      }
    }
    return toReturn;
  },
  formatCurrency (input) {
    return parseInt(input).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    // return '฿' + parseInt(input).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
}
