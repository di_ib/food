export default {
  getters: {
    async merge(obj1, obj2) {
			return await this.recursive(obj1, obj2)
    },
		recursive (obj1, obj2) {
			for (var p in obj2)
			{
					try {

						if (obj2[p].constructor == Object) {
							obj1[p] = recursive(obj1[p], obj2[p]);
						} else {
							obj1[p] = obj2[p];
						}

					} catch (e) {
						obj1[p] = obj2[p];
					}
			}

			return obj1
		}
  }
}
