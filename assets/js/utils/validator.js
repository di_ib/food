export default {
  async make (input, check) {

    let data
    try {
      let items = {}
      for (var i in check)
        {
          let obj = this.bb(input, i, check)

          if (obj.length) {
            items[i] = obj
          }
        }

        if (Object.keys(items).length) throw { items }

        data = input

    } catch (err) {
        throw err
    }

      return data
  },
  bb (input, i, check) {

    	let text = []
    	let x = check[i].split('|')

    	// step 1
    	if (x.indexOf('required') != -1)
    	{
    		var b = this.required(i, input[i])
    		if (b.err) text.push(b.message)
    	}

    	// step 2
    	if (x.indexOf('email') != -1)
    	{
    		var b = this.email(i, input[i])
    		if (b.err) text.push(b.message)
    	}

    	// step 3
    	if (x.indexOf('password') != -1)
    	{
    		var b = this.password(i, input[i])
    		if (b.err) text.push(b.message)
    	}

    	// step 4
    	if (x.indexOf('match') != -1)
    	{
    		var b = this.match(i, input[i], input['password'])
    		if (b.err) text.push(b.message)
    	}

      // step 5
      if (x.indexOf('charCount') != -1)
      {
        var b = this.charCount(i, input[i])
        if (b.err) text.push(b.message)
      }

      // step 6
      if (x.indexOf('number') != -1)
      {
        var b = this.number(i, input[i])
        if (b.err) text.push(b.message)
      }

    	return text;
  },
  email (i, value) {

    	if (/^[a-z0-9][a-z0-9-_\.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/.test(value))
    	{
    	   return { err: false }
    	} else {
    		return { err: true, message: `Invalid email format (Ex:example@example.com)` }
    	}
  },
  required (i, value) {

    if (value.length == 0)
    {
      return { err: true, message: `${i} is required` }
    } else {
      return { err: false }
    }
  },
  password (i, value) {

    if (/^([a-z0-9])+$/i.test(value))
    {
        return { err: false }
    } else {
        return { err: true, message: `name of password` }
    }
  },
  match(i, value1, value2) {
    if(value1 != value2) {
      return { err: true, message: `Password not match!`}
    }else {
      return { err: false }
    }

  },
  charCount (i, value) {

    if(value.length < 8){
      return { err: true, message: `Password must greater than 8 character.` }
    } else {
      return { err: false }
    }
  },
  number (i, value) {
    if (/^\d+$/.test(value))
    {
       return { err: false }
    } else {
      return { err: true, message: `Number only` }
    }
  }
}
