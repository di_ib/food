import axios from 'axios'
import store from '@/store'

export default () => {
  const instance = axios.create({
    baseURL: store().state.base.api,
    withCredentials: false,
    headers: {
      'x-id': store().getters.tax._id,
      'Accept': 'application/json;charset=UTF-8',
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': 'Bearer ' + store().getters.auth.access_token
    }
  })

  // add a request interceptor
  instance.interceptors.request.use(config => {
    return config
  }, err => {
    return Promise.reject(err)
  })

  // add a response interceptor
  instance.interceptors.response.use(response => {
    return response
  }, err => {
    if (err.response.status == 401)
    {
      return axios.post(store.state.base.api + '/auth/refresh/token', { refresh_token: store.getters.auth.refresh_token }).then(({ data }) => {
        window.localStorage.setItem('auth', JSON.stringify(Object.assign(data.user, { access_token: data.access_token, refresh_token: data.refresh_token })))
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token
      })
    }
    return Promise.reject(err)
  })
  return instance
}
