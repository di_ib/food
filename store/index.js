import Vue from 'vue'
import Vuex from 'vuex'

import auth from './store/auth'
import show from './store/show'

Vue.use(Vuex)

export const store = () => {
  return new Vuex.Store({
  modules: {
    auth, show
  },
  state: {
  	base: {
      api: process.env.NODE_ENV === 'production' ? 'https://api.nooxnoox.com' : 'http://localhost:3000'
    },
    modal: { open: false, size: '', component: '', input: {}, options: {}},
  },
  mutations: {
    modalAssign (state, data) {
      Object.assign(state.modal, {...data, input: Object.assign({}, JSON.parse(JSON.stringify(data.input))) })
    }
  },
  actions: {

  },
  getters: {
    base: state => state.base,
    modal: state => state.modal
  }
})
}

export default store
