const initial = { _id: null, picture: '', name: '', access_token: null, refresh_token: null }

export default {

  state: {
    auth: JSON.parse(window.localStorage.getItem('auth') || JSON.stringify(initial)),
    tax: JSON.parse(window.localStorage.getItem('tax') || JSON.stringify({ _id: null })),
    share: JSON.parse(window.localStorage.getItem('share') || JSON.stringify([])),
  },
  mutations: {
    authLogin (state, obj) {
      localStorage.setItem('auth', JSON.stringify(obj))
      Object.assign(state.auth, obj)
      console.log(state.auth);
    },
    authLogout (state) {
      localStorage.setItem('auth', JSON.stringify(initial))
      localStorage.removeItem('isLogIn')
      localStorage.clear()
      Object.assign(state
        .auth, initial)
    },
    authShare (state, data) {
      localStorage.setItem('share', JSON.stringify(data))
      state.share = data
    },
    authTax (state, data) {
      console.log(data);
      localStorage.setItem('tax', JSON.stringify(data))
      Object.assign(state.tax, data)
    },
  },
  getters: {
    auth: state => state.auth,
    tax: state => state.tax,
    share: state => state.share
  }
}
