export default {

  state: {
    data: {},
    item:{},
    basket:JSON.parse(window.localStorage.getItem('basket') || JSON.stringify([])),
    stores: JSON.parse(window.localStorage.getItem('store') || JSON.stringify({ })),
    responsive: JSON.parse(window.localStorage.getItem('responsive') || JSON.stringify({ })),

  },

  mutations: {
    showAssign (state, obj) {
      state.data = obj
    },
    itemAssign (state, obj) {
      state.item = obj
    },
    basketAssign (state, obj) {
      localStorage.setItem('basket', JSON.stringify(obj))
      state.basket = obj
    },
    storeAssign (state, obj) {
      localStorage.setItem('store', JSON.stringify(obj))
      state.stores = obj
    },
    responsiveAssign (state, obj) {
      localStorage.setItem('responsive', JSON.stringify(obj))
      state.responsive = obj
    }
  },
  getters: {
    show: state => state.data,
    item: state => state.item,
    stores: state => state.stores,
    basket: state => state.basket,
    responsive: state => state.responsive,
  }
}
